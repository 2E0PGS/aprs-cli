#/bin/bash

# Termux Android termux-location CLI parser for aprs-cli.

# Copyright (C) <2019-2020>  <Peter Stevenson> (2E0PGS)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

list_help_commands() {
	echo "Termux Android termux-location CLI parser for aprs-cli."
	echo ""
	echo "List of commands:"
	echo "--send-position <username> <password> <SSID> <symbol ID> <symbol code> <optional comment> Sends a position packet to APRS-IS network."
}

if [ "$1" == "--help" ]; then
	list_help_commands
elif [ "$1" == "--send-position" ]; then
	location_data=$(termux-location)
	lat=$(echo "$location_data" | grep "lat" | cut -d ':' -f 2 | cut -d ',' -f 1 | cut -d ' ' -f 2)
	long=$(echo "$location_data" | grep "long" | cut -d ':' -f 2 | cut -d ',' -f 1 | cut -d ' ' -f 2)

	./aprs-cli.sh --send-position "$2" "$3" "$4" "$lat" "$long" "$5" "$6" "$7"
else
	list_help_commands
fi
