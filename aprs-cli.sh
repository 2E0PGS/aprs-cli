#!/bin/bash

# APRS CLI a simple bash client for the APRS-IS API.

# Copyright (C) <2019-2020>  <Peter Stevenson> (2E0PGS)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Useful information
# Latitude is expressed as a fixed 8-character field, in degrees and decimal minutes (to two decimal places), followed by the letter N for north or S for south.
# Longitude is expressed as a fixed 9-character field, in degrees and decimal minutes (to two decimal places), followed by the letter E for east or W for west.
# ; represents an object.
# = represents a location without timestamp.
# Seperator between lat and long is the first part of a symbol identifier.
# Seperator between the lat long and comment is the latter part of the symbol identifier.
# *111111z represents a static object which prevents conflicts with identically named objects across the map.
# It's important there is an appropriate amount of spaces before the object timestamp because object name is fixed 9 chars long:obname    *111111z
# * Indicates a live object.
# Comment is max 43 chars.
# API spec: http://www.aprs.org/doc/APRS101.PDF

# Convert the decimal degress which are more common to degrees minutes and seconds.
# $2 dictates if your converting a lat or a long string.
degree_decimal_to_dms() {
	orig_lat="$1"
	orig_lat_positive="$(echo $orig_lat | cut -f2 -d"-")"
	degrees="$(echo $orig_lat_positive | cut -f1 -d".")"
	mins="$(echo "($orig_lat_positive - $degrees) * 60" | bc -l | cut -c 1-5)"
	#seconds="$(echo "($orig_lat_positive - $degrees - $mins/60) * 3600" | bc -l | cut -f1 -d".")"
	if [ $(echo "$orig_lat > 0" | bc -l) -eq 1 ]; then
		if [ $2 == 0 ]; then
			lat_n_s="N"
		else
			lat_n_s="E"
		fi
	else
		if [ $2 == 0 ]; then
			lat_n_s="S"
		else
			lat_n_s="W"
		fi
	fi
	echo "$degrees$mins$lat_n_s"
}

list_help_commands() {
	echo "APRS CLI a simple bash client for the APRS-IS API."
	echo ""
	echo "List of commands:"
	echo "--send-position <username> <password> <SSID> <lat> <long> <symbol ID> <symbol code> <optional comment> Sends a position packet to APRS-IS network."
	echo "--send-status <username> <password> <SSID> <status>                                                    Sends a status update packet to APRS-IS network."
	#echo "--send-object <username> <password> <SSID> <object SSID> <lat> <long> <optional comment> Sends a object position packet to APRS-IS network."
}

if [ "$1" == "--help" ]; then
	list_help_commands
elif [ "$1" == "--send-position" ]; then
	lat_dms=$(degree_decimal_to_dms $5 0)
	lat_dms_padded="$(printf '%08s' "$lat_dms")"
	lat_dms_padded_zero=${lat_dms_padded//' '/'0'}
	long_dms=$(degree_decimal_to_dms $6 1)
	long_dms_padded="$(printf '%09s' "$long_dms")"
	long_dms_padded_zero=${long_dms_padded//' '/'0'}
	payload="$lat_dms_padded_zero$7$long_dms_padded_zero$8$9"
	echo $lat_dms
	echo $long_dms
	# Format is something like user <username> pass <password> vers <app name> <version number> filter <filter> \r\n <path> <lat> <symbol1> <long> <symbol2> <comment> \r\n
	printf "user "$2" pass "$3" vers aprscli 1 filter r/33.25/-96.5/50\r\n"$4">APRS,TCPIP*:=""$payload""\r\n" | nc -w 1 euro.aprs2.net 14580
elif [ "$1" == "--send-status" ]; then
	printf "user "$2" pass "$3" vers aprscli 1 filter r/33.25/-96.5/50\r\n"$4">APRS,TCPIP*:>"$5"\r\n" | nc -w 1 euro.aprs2.net 14580
#elif [ "$1" == "--send-object" ]; then
#	printf "user "$2" pass "$3" vers aprscli 1 filter r/33.25/-96.5/50\r\n"$4">APRS,TCPIP*:;"$5    *111111z$lat_dms_padded$7$long_dms_padded$8$9"\r\n" | nc -w 1 euro.aprs2.net 14580
else
	list_help_commands
fi
