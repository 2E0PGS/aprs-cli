# APRS CLI

Simple CLI for APRS TCP. Written in bash. Requires `netcat` and `bc`.

Useful for static placement of APRS objects, mobile work via Termux or other CLI input.

## Termux

Also included is a Termux Android `termux-location` CLI parser for `aprs-cli`. This is useful as a cron powered APRS client for Android phones which runs solid 24/7 if required.

If using the `termux-location-aprs-cli.sh` helper script you will need to install [Termux](https://play.google.com/store/apps/details?id=com.termux&hl=en_GB) and [Termux:API](https://play.google.com/store/apps/details?id=com.termux.api&hl=en_GB) Android apps. 

After installing Termux:API APK you need to run `apt install termux-api` in the bash shell.

Please read their documentation first for that specific use case to ensure Termux haven't change anything since I last updated my notes.

You will probably need to grant location permission first time by running `termux-location`.

### Cron examples

```
*/5 * * * * cd /data/data/com.termux/files/home/aprs-cli; ./termux-location-aprs-cli.sh --send-position 2E0PGS PASS123 2E0PGS-1 / [ aprs-cli
```

## Repeater and other static object placement

Currently you can place normal users static or mobile on the map. 

I plan to add support for `objects` which are special and can be used to place a repeater/tower etc statically on the map.